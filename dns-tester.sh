#!/bin/bash

dns_servers_file="./dns-servers.txt"
lookup_hosts_file="./lookup-hosts.txt"
failures_file="./failures.csv"

if [ ! -f "$dns_servers_file" ]; then
  echo "Error: $dns_servers_file not found."
  exit 1
fi

if [ ! -f "$lookup_hosts_file" ]; then
  echo "Error: $lookup_hosts_file not found."
  exit 1
fi

perform_nslookup() {
  host="$1"
  server="$2"
  result=$(nslookup "$host" "$server" 2>&1)
  if [[ "$result" == *"NXDOMAIN"* || "$result" == *"SERVFAIL"* || "$result" == *"can't find"* ]]; then
    echo "$(date),$host,$server" >> "$failures_file"
  fi
}

while [ 1 = 1 ]; do
  while IFS= read -r dns_server || [[ -n "$dns_server" ]]; do
   [ -z "$dns_server" ] && continue
   while IFS= read -r lookup_host || [[ -n "$lookup_host" ]]; do
     [ -z "$lookup_host" ] && continue
     perform_nslookup "$lookup_host" "$dns_server"
   done < "$lookup_hosts_file"
  done < "$dns_servers_file"
  sleep 60
done