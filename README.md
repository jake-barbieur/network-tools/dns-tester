# dns-tester

A script to test DNS queries against various DNS servers and log failures.  Add DNS servers to dns-servers.txt and hosts to look up to lookup-hosts.txt.  
Failures are written to failures.csv.